package principal;

import java.util.List;

import jakarta.persistence.*;
import principal.daos.PessoaDAO;
import principal.modelos.Pessoa;

public class Programa {
	
	public static void main(String[] args) {
		
		PessoaDAO pDao = new PessoaDAO();		
			
		//Criação do objeto pessoa
		Pessoa p = new Pessoa("João", "3333333");		
		//salvar(p);
		
		//Buscar pessoa pelo ID
		Pessoa p2 = pDao.buscarPorId(6);
		
		System.out.println();
		System.out.println("---------------");
		
		System.out.println("Id: " + p2.getId() 
		        + " Nome: " + p2.getNome() 
		        + " CPF: " + p2.getCpf());
		
		System.out.println("---------------");
		System.out.println();
		
		
		//Atualizar os dados de uma pessoa
		p2.setNome("João Paulo");
		//atualizar(p2);
		
		
		//Excluir pessoa do banco de dados
		//pDao.apagar(5);

		System.out.println("LISTA DE PESSOAS");
		//Listar pessoas no banco
		List<Pessoa> lista = pDao.listar();
		for (Pessoa pessoa : lista) {
			System.out.println("Id: " + pessoa.getId() 
			                 + " Nome: " + pessoa.getNome() 
			                 + " CPF: " + pessoa.getCpf());
		}
		
		System.out.println("---------------");
		System.out.println();
		
		//Encerrar a conexão
		pDao.close();	
	}
}
